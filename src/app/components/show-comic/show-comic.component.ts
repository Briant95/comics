import { Component, OnInit } from '@angular/core';
import { ComicService } from 'src/app/providers/comic.service';

@Component({
  selector: 'app-show-comic',
  templateUrl: './show-comic.component.html',
  styleUrls: ['./show-comic.component.less']
})
export class ShowComicComponent implements OnInit {

  arrayComics = [];
  search = false;

  constructor(private RandomComic: ComicService) { }

  ngOnInit() {
    // iterate comics
    for (let i = 0; i < 50; i++) {
      // endpoint comics
      this.RandomComic.getComic().then((comic: any) => {
        this.arrayComics.push(comic);
      });
    }
  }
}
