import { Component, OnInit } from "@angular/core";
import { ComicService } from "src/app/providers/comic.service";

@Component({
  selector: "app-comic",
  templateUrl: "./comic.component.html",
  styleUrls: ["./comic.component.less"]
})
export class ComicComponent implements OnInit {
  selectYearComic = [];
  yearsComics = [];

  search = true;
  selectComic = 0;

  searchComic: any[] = [];

  constructor(private RandomComic: ComicService) {}

  ngOnInit() {
    // iterate comics
    for (let i = 0; i < 50; i++) {
      // endpoint comics
      this.RandomComic.getComic().then((comic: any) => {
        this.yearsComics.push(comic.year);
        this.yearsComics = this.yearsComics.filter((value, index, self) => {
          return self.indexOf(value) === index;
        });
      });
    }
  }

  SelectComic(event) {
    this.search = false;
    for (let i = 0; i < 50; i++) {
      this.RandomComic.getComic().then((yearComic: any) => {
        if (yearComic.year == event) {
          this.selectYearComic.push(yearComic);
          this.searchComic = this.selectYearComic;
        }
      });
    }
  }
}
