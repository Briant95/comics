import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComicComponent } from './components/comic/comic.component';
import { ShowComicComponent } from './components/show-comic/show-comic.component';

const routes: Routes = [
  {
    path: '',
    component: ComicComponent
  },
  {
    path: 'comics',
    component: ComicComponent
  },
  {
    path: 'search',
    component: ShowComicComponent
  },
  { path: '',
    redirectTo: 'comics',
    pathMatch: 'full'
  },
  { path: '**', component: ComicComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
