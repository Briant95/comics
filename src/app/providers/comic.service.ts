import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

const urlCors = 'https://cors-anywhere.herokuapp.com/';

@Injectable({
  providedIn: 'root'
})
export class ComicService {

  constructor(private http: HttpClient) {}

  getComic() {

  let random = Math.floor(Math.random() * (614 - 1)) + 1;

  	return this.http.get(urlCors + "https://xkcd.com/" + random + "/info.0.json").toPromise();

  }

}
