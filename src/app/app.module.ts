import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComicComponent } from './components/comic/comic.component';
import { SearchComicComponent } from './components/search-comic/search-comic.component';
import { ShowComicComponent } from './components/show-comic/show-comic.component';

@NgModule({
  declarations: [
    AppComponent,
    ComicComponent,
    SearchComicComponent,
    ShowComicComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
