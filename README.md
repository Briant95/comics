# Comic

  This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.1.

## Prerequisites

  Angular requires Node.js version 10.9.0 or later.

    * To check your version, run node -v in a terminal/console.

    * To get Node.js, go to nodejs.org.


 ## npm package manager

  Angular, the Angular CLI, and Angular apps depend on features and functionality provided by libraries that are available as npm packages. To download and install npm packages, you must have an npm package manager.

  This setup guide uses the npm client command line interface, which is installed with Node.js by default.

  To check that you have the npm client installed, run npm -v in a terminal/console.


## Install the Angular CLI

  run command `npm install -g @angular/cli`


## Clone repository

  git clone https://Briant95@bitbucket.org/Briant95/comics.git


## install packcakes

  npm install or npm i


## Development server

  Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

